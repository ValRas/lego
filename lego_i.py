import sys
from PyQt5.QtWidgets import QApplication, QWidget
from sorter_design import  lego_app


if __name__ == '__main__':

    app = QApplication(sys.argv)

    w = lego_app()
    w.show()

    sys.exit(app.exec_())