from PyQt5.QtWidgets import QMainWindow
from design import Ui_MainWindow
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtCore import QTimer

import cv2

from classification import LegoClass

class lego_app(QMainWindow, Ui_MainWindow):

    def __init__(self):

        super().__init__()


        self.setupUi(self)  # Это нужно для инициализации нашего дизайна
        self.timer_frame = QTimer()
        self.timer_frame.timeout.connect(self.update_frame)

        self.show_waiting_pic()

        self.capture = cv2.VideoCapture(1)
        self.x1, self.y1, self.x2, self.y2 = 50, 50, 300, 300

        self.mask = cv2.imread(r"C:\PyProjects\Lego\mask.jpg")
        self.brain = LegoClass()
        self.prediction = ""
        self.count = 0

        self.startButton.clicked.connect(self.start)
        self.stopButton.clicked.connect(self.stop)
        self.saveButton.clicked.connect(self.save_mask)

        self.leftButton.clicked.connect(self.mask_left)
        self.rightButton.clicked.connect(self.mask_right)
        self.downButton.clicked.connect(self.mask_down)
        self.upButton.clicked.connect(self.mask_up)

    #region buttons mask

    def mask_left(self):
        if self.x1 > 50:
            self.x1 = self.x1 - 10
            self.x2 = self.x2 - 10

    def mask_right(self):
        if self.x2 < 590:
            self.x1 = self.x1 + 10
            self.x2 = self.x2 + 10

    def mask_up(self):
        if self.y1 > 50:
            self.y1 = self.y1 - 10
            self.y2 = self.y2 - 10

    def mask_down(self):
        if self.y2 < 430:
            self.y1 = self.y1 + 10
            self.y2 = self.y2 + 10



    def save_mask(self):
        ret, frame = self.capture.read()
        if ret:
            detail = frame[self.y1:self.y2, self.x1:self.x2]
            cv2.imwrite(r"C:\PyProjects\Lego\mask.jpg",detail)
            self.mask = detail.copy()

    #endregion

    #region control buttons
    def stop(self):
        self.timer_frame.stop()
        self.show_waiting_pic()

    def start(self):
        mask = cv2.imread(r"C:\PyProjects\Lego\mask.jpg")

        self.timer_frame.start(60)

    #endregion

    #region image
    def show_waiting_pic(self):
        frame = cv2.imread(r"C:\PyProjects\Lego\load.jpg")
        self.set_image(frame)

    def update_frame(self):

        ret,frame = self.capture.read()
        if not ret:
            self.label.setStyleSheet('color: red')
            self.label.setText("Ошибка чтения данных с камеры...")
            return

        if self.count ==0:
            self.prediction = self.get_prediction(frame[self.y1:self.y2, self.x1:self.x2].copy())

        cv2.rectangle(frame, (self.x1, self.y1), (self.x2, self.y2), (0, 255, 0), 2)
        cv2.putText(frame, self.prediction, (self.x1, self.y1 - 10), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (240, 240, 240), 1)

        self.set_image(frame)

        if self.count > 9:
            self.count = 0
        else:
            self.count+= 1




    def get_prediction(self,img):

        diff = cv2.absdiff(self.mask, img)
        diff = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
        diff = cv2.GaussianBlur(diff, (5, 5), 3)
        diff = cv2.Canny(diff, 10, 50)

        return self.brain.recognize(diff)

    def set_image(self,image):

        y_size = 240
        x_size = 320

        image = cv2.resize(image,(x_size,y_size))


        bytesPerLine = 3 * x_size

        image = QImage(image.data, x_size, y_size,bytesPerLine, QImage.Format_RGB888)
        image = image.rgbSwapped()

        pixmap = QPixmap.fromImage(image)
        self.label.setPixmap(pixmap)

    #endregion