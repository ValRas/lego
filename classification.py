#from google.colab import drive
import glob
import cv2
import numpy as np

import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.callbacks import ModelCheckpoint,EarlyStopping
from keras.models import load_model as keras_load_model

#drive.mount("/content/drive")
# data_files = glob.glob("/content/drive/My Drive/Lego/json/*.*")
# for data_file in data_files:
#   print(data_file)

class LegoClass():
    def __init__(self):
        self.model = Model_Lego2()
        self.model.load_weights(r"C:\PyProjects\Lego\models\lego-1.h5")

    def recognize(self,detail):
        img = detail.copy()
        if img.shape[0]!=250 or img.shape[1]!=250:
            img = cv2.resize(img,(250,250))
        if len(img.shape) ==3:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        x = np.asarray(img / 255)
        x = tf.expand_dims(x, 2)
        x = tf.expand_dims(x, 0)

        answer = self.model.predict(x)[0]

        if answer[0] > 0.5:
            return('1 detail  2x1, prob' + str((int(answer[0] * 100))))
        if answer[1] > 0.5:
            return("2 detail 2x2, prob"  + str((int(answer[1] * 100))))
        if answer[2] > 0.5:
            return("3 detail 2x4, prob"+ str((int(answer[2] * 100))))

        return 'NLO'

def get_img_generators():

        train_path = "/content/drive/My Drive/Lego/train"
        validate_path = "/content/drive/My Drive/Lego/test"

        img_width, img_height = 250, 250

        epochs = 50
        steps_per_epoch = 50


        datagen_kwargs = dict(rescale=1. / 255, validation_split=.20)
        dataflow_kwargs = dict(target_size=(img_height, img_width), batch_size=16,
                               interpolation="bilinear",class_mode="categorical",color_mode="grayscale")

        valid_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
            **datagen_kwargs)
        valid_generator = valid_datagen.flow_from_directory(
            validate_path, subset="validation", shuffle=False, **dataflow_kwargs)

        train_datagen = tf.keras.preprocessing.image.ImageDataGenerator(
                rotation_range=40,
                horizontal_flip=True,
                vertical_flip = True,
                width_shift_range=0.2, height_shift_range=0.2,
                shear_range=0.2, zoom_range=0.2,
                **datagen_kwargs)

        train_generator = train_datagen.flow_from_directory(
            train_path, subset="training", shuffle=True, **dataflow_kwargs)

        return (train_generator,valid_generator)

def Model_Lego2():
  model = keras.Sequential([
      keras.layers.Conv2D(16, 3, padding='same', activation='relu', input_shape=(250, 250, 1)),
      keras.layers.MaxPooling2D(),
      keras.layers.Conv2D(32, 3, padding='same', activation='relu'),
      keras.layers.MaxPooling2D(),
      keras.layers.Conv2D(64, 3, padding='same', activation='relu'),
      keras.layers.MaxPooling2D(),
      keras.layers.Flatten(),
      keras.layers.Dense(512, activation='relu'),
      keras.layers.Dense(128, activation='relu'),
      keras.layers.Dense(3, activation='softmax')
  ])
  model.compile(optimizer='adam',
              loss='categorical_crossentropy',
              metrics=['acc'])
  return model

def Model_Lego():
        model = keras.applications.InceptionV3(include_top=False, weights='imagenet', input_shape=(200, 200, 3))

        for layer in model.layers:
            layer.trainable = False

        x = model.output
        x = keras.layers.Flatten()(x)
        x = keras.layers.Dense(1024, activation="relu")(x)
        x = keras.layers.Dropout(0.5)(x)
        x = keras.layers.Dense(1024, activation="relu")(x)
        predictions = keras.layers.Dense(3, activation="softmax")(x)

        # creating the final model
        model_final = keras.Model(model.input, predictions)

        # compile the model
        model_final.compile(loss="categorical_crossentropy", optimizer='adam',
                            metrics=["accuracy"])
        return model_final

# lego_model = Model_Lego2()
# lego_model.load_weights(r"C:\PyProjects\Lego\lego-3107.h5")
# #lego_model.summary()
# #print(lego_model.summary())
#
# #lego_model = keras_load_model(r"C:\PyProjects\Lego\lego.h5")
#
# image = cv2.imread(r"C:\PyProjects\Lego\t8.jpg")
# #image = cv2.resize(image,(250,250))
# gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
# x = np.asarray(gray/255)
# x = tf.expand_dims(x, 2)
# x = tf.expand_dims(x, 0)
#
# answer = lego_model.predict(x)[0]
#
# if answer[0] > 0.5:
#     print('1 кубик 2x1')
#     print(int(answer[0]*100))
# if answer[1] > 0.5:
#     print("2 кубик 2x2")
#     print(int(answer[1] * 100))
# if answer[2] > 0.5:
#     print("3 кубик 2x4")
#     print(int(answer[2] * 100))



#frame = cv2.imread("/content/drive/My Drive/Lego/test/Brick22/0001.png")

# train_generator,validation_generator = get_img_generators()
# print(train_generator.samples)
#
#
# model_temp_file = "/content/drive/My Drive/Lego/models/lego.h5"
# labels_temp_file = "/content/drive/My Drive/Lego/models/lego_labels.txt"
#
# epochs = 50
# steps_per_epoch = 50
#
# checkpoint = ModelCheckpoint(model_temp_file, monitor='val_accuracy', verbose=1, save_best_only=True,
#                                                  save_weights_only=False, mode='auto', save_freq=1)
#
#
# early = EarlyStopping(monitor='val_accuracy', min_delta=0, patience=10, verbose=1, mode='auto')
# lego_model.fit(
#                         train_generator,
#                         epochs=10, batch_size=64,
#                         steps_per_epoch = 32,
#                         validation_data=validation_generator,
#
#                         callbacks=[checkpoint, early])
#
# lego_model.save(model_temp_file)
#print(lego_model.summary())
